<?php
require "config.php";
class DAO
{
    protected $pdo;

    protected $hasActiveTransaction = false;

    public function __construct()
    {
//        $this->pdo = new PDO(
//            'mysql:host=' . $GLOBALS['db_host'] . ';dbname=' . $GLOBALS['db_dbname'],
//            $GLOBALS['db_username'], $GLOBALS['db_password']);
        $this->pdo = new PDO('sqlite:'.$GLOBALS['sqlite_db_file']);
        $this->pdo->query('set names utf8;');
    }

    public function beginTransaction()
    {
        if($this->hasActiveTransaction)
        {
            return false;
        }
        else
        {
            $this->hasActiveTransaction = $this->pdo->beginTransaction();
            $this->pdo->query('set names utf8;');
        }
    }

    public function commit()
    {
        $this->pdo->commit();
        $this->hasActiveTransaction = false;
    }

    public function rollback()
    {
        $this->pdo->rollBack();
        $this->hasActiveTransaction = false;
    }

    public function query($sql)
    {
        $rs = $this->pdo->query($sql)
            ->fetchAll();
        return $rs;
    }

    public function insert($sql)
    {
        $this->pdo->exec($sql);
        $temp = $this->pdo->lastInsertId();
        return $temp;
    }

    public function update($sql)
    {
        return $statement = $this->pdo->query($sql);
    }

    public function delete($sql)
    {
        $statement = $this->pdo->query($sql);
    }

    public function getOne($sql)
    {
        return $this->pdo->query($sql)->fetch();
    }

    public function count($sql)
    {
        return $this->pdo->query($sql)->rowCount();
    }
}
